const PASSWORD = "4862";


const initialState = {
    currentCombo: '',
    status: "default"
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "ADD_NUMBER":
            console.log(state.currentCombo + action.value);
            if (state.currentCombo.length > 3) {
                console.log("Max number reached");
                break;
            }
            return { ...state, currentCombo: state.currentCombo + action.value }
        case "REMOVE_NUMBER":
            return { ...state, currentCombo: state.currentCombo.slice(0, -1) }

        case "CHECK":       
            if (state.currentCombo === PASSWORD) {
                return { ...state, currentCombo: "GRANTED", status: "granted" }
            } 
            else {
                return { ...state, currentCombo: "DENIED", status:"denied"}
            }
        case "CLEAR":
            return {...state, currentCombo:"", status:"default"}
        default:
            return state;
    }

    return state;
};

export default reducer;