import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from '../../components/Button/Button';
import './Lock.css';


const Lock = () => {
    const dispatch = useDispatch();
    const displayCombo = useSelector(state => state.currentCombo);
    const status = useSelector(state =>state.status);
    const enterNumber = num => dispatch({ type: "ADD_NUMBER", value: num });
    const removeNumber = () => dispatch({ type: "REMOVE_NUMBER" });
    const checkPassword = () => dispatch({ type: "CHECK" });
    const clearField = () =>dispatch({type:"CLEAR"})

    const shownMessage = mess =>{
        if (mess.length >4 ) {
            return mess
        } else{
            return "*".repeat(mess.length)
        }
    }
    const message = shownMessage(displayCombo)
    return (
        <div className="Lock">
            <div className={"Password " + status}>{message}</div>
            <div className="Numpad">
                <Button onClick={() => enterNumber(7)}>7</Button>
                <Button onClick={() => enterNumber(8)}>8</Button>
                <Button onClick={() => enterNumber(9)}>9</Button>
                <Button onClick={() => enterNumber(4)}>4</Button>
                <Button onClick={() => enterNumber(5)}>5</Button>
                <Button onClick={() => enterNumber(6)}>6</Button>
                <Button onClick={() => enterNumber(1)}>1</Button>
                <Button onClick={() => enterNumber(2)}>2</Button>
                <Button onClick={() => enterNumber(3)}>3</Button>

                <Button onClick={removeNumber}>{"<"}</Button>
                <Button onClick={() => enterNumber(0)}>0</Button>
                <Button onClick={checkPassword}>E</Button>
                <Button onClick={clearField}>CLEAR</Button>
            </div>
        </div>
    );
};

export default Lock;